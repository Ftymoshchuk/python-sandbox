# Define a simple chatbot with predefined responses
chatbot_responses = {
    "hi": "Hello!",
    "how are you?": "I'm doing well, thank you!",
    "bye": "Goodbye!",
    # Add more responses as needed
}

def get_response(message):
    # Convert message to lowercase for case-insensitive matching
    message = message.lower()
    
    # Look up response for the input message, or default to a generic response
    return chatbot_responses.get(message, "I'm not sure how to respond to that.")
