function sendMessage() {
    var userInput = document.getElementById("user-input").value;
    document.getElementById("user-input").value = "";

    var chatBox = document.getElementById("chat-box");
    chatBox.innerHTML += "<p><strong>You:</strong> " + userInput + "</p>";

    // Send user input to the server and get response
    fetchResponse(userInput);
}

function displayResponse(response) {
    var chatBox = document.getElementById("chat-box");
    chatBox.innerHTML += "<p><strong>Chatbot:</strong> " + response + "</p>";
}

function fetchResponse(userInput) {
    // Send user input to the server (not implemented in this example)
    // For simplicity, we'll directly call the chatbot logic in Python
    // Replace this with actual server communication in a real-world application
    var response = getChatbotResponse(userInput);
    displayResponse(response);
}

function getChatbotResponse(userInput) {
    // Replace this with actual server communication in a real-world application
    // For this example, we'll call a Python function directly using Skulpt (a Python interpreter in JavaScript)
    return Sk.importMainWithBody("<stdin>", false, "chatbot.py", true, userInput);
}
